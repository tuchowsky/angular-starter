import { Component, OnInit } from '@angular/core';
import { SideNavService } from 'src/app/services/side-nav/side-nav.service';

@Component({
  selector: 'app-side-nav',
  templateUrl: './side-nav.component.html',
  styleUrls: ['./side-nav.component.scss']
})
export class SideNavComponent implements OnInit {
  isNavVisible: boolean;

  constructor(private sideNavService: SideNavService) { 
    this.isNavVisible = this.sideNavService.isNavOpenSource.value;
  }

  ngOnInit(): void {}

  onNavToggle(): void {
    this.sideNavService.toggleNav();
    this.sideNavService.isNavOpen.subscribe(data => {
      this.isNavVisible = data;
    });
  }
}
