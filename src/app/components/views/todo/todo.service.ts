import { Injectable } from '@angular/core';
import { TodoItemInterface } from './todo.interface';
import { TODO_LIST } from './todo.mock';

@Injectable({
  providedIn: 'root'
})
export class TodoService {
  todoList: Array<TodoItemInterface> = [];

  constructor() {
    this.todoList = TODO_LIST;
   }

  getTodoList(): Array<TodoItemInterface> {
    return this.todoList;
  }

  addTodoItem(todo): void {
    const id = this.todoList.length - 1;
    console.log(id, 'id');
    this.todoList.push({
      id,
      ...todo
    });
  }

  deleteTodoItem(index): void {
    this.todoList.splice(index, 1);
  }
}
