import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { TodoItemInterface } from '../todo.interface';

@Component({
  selector: 'app-todo-item',
  templateUrl: './todo-item.component.html',
  styleUrls: ['./todo-item.component.scss']
})
export class TodoItemComponent implements OnInit {
  @Input() todo: TodoItemInterface;
  @Output() removeItem = new EventEmitter<Event>();

  constructor() { }

  ngOnInit(): void {
    console.log(this.todo);
  }

  removeTodoClick(event: Event): void {
    // event.stopPropagation();
    this.removeItem.emit(event);
  }
}
