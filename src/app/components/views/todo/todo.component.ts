import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { SideNavService } from 'src/app/services/side-nav/side-nav.service';
import { TodoItemInterface } from './todo.interface';
import { TODO_LIST } from './todo.mock';
import { TodoService } from './todo.service';

@Component({
  selector: 'app-todo',
  templateUrl: './todo.component.html',
  styleUrls: ['./todo.component.scss']
})

export class TodoComponent implements OnInit {
  isNavOpen: boolean;
  todoList: Array<TodoItemInterface> = [];
  todoForm: FormGroup;

  constructor(private sideNavService: SideNavService, 
              private todoService: TodoService) {
    this.sideNavService.isNavOpen.subscribe(data => {
      this.isNavOpen = data;
    });
  }

  ngOnInit(): void {
    this.todoList = this.todoService.getTodoList();
    this.todoForm = new FormGroup({
      'description': new FormControl(null, Validators.required)
    });
  }

  onAddTodo(): void {
    const value = this.todoForm.value;
    this.todoForm.reset();
    this.todoService.addTodoItem({
      ...value,
      isDone: false,
      date: new Date()
    });
  }

  onDeleteTodo(index): void {
    this.todoService.deleteTodoItem(index);
  }
  onFieldEdit(i): void {
    console.log(i);
  }
}
