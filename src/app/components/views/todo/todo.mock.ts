import { TodoItemInterface } from './todo.interface';

export const TODO_LIST: Array<TodoItemInterface> = [
  {
    id: 0,
    description: 'First item',
    isDone: false,
    date: new Date()
  },
  {
    id: 1,
    description: 'Second item',
    isDone: true,
    date: new Date()
  },
  {
    id: 2,
    description: 'Third item',
    isDone: true,
    date: new Date()
  }
];
