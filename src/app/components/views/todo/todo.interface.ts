export interface TodoItemInterface {
    id: number;
    description: string;
    isDone: boolean;
    date: Date;
}
