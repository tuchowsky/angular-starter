import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import {SideNavService} from '../../../services/side-nav/side-nav.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  isActive = false;
  isNavOpen: boolean;
  subscription: Subscription;

  constructor(private sideNavService: SideNavService) {
    this.subscription = this.sideNavService.isNavOpen.subscribe(data => {
      this.isNavOpen = data;
    });
  }

  ngOnInit(): void { }

  changeColor(): void {
    this.isActive = !this.isActive;
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }
}
