import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { SideNavService } from 'src/app/services/side-nav/side-nav.service';
import { ApiService } from './api.service';

@Component({
  selector: 'app-api',
  templateUrl: './api.component.html',
  styleUrls: ['./api.component.scss']
})
export class ApiComponent implements OnInit {
  isNavOpen: boolean;
  userList: any;

  constructor(
    private sideNavService: SideNavService, 
    private apiService: ApiService) {
    this.sideNavService.isNavOpen.subscribe(data => {
      this.isNavOpen = data;
    });
    this.apiService.apiData.subscribe(data => {
      this.userList = data;
    });
  }

  ngOnInit(): void { }

  onSendRequest() {
    this.apiService.getData();
    console.log(this.userList);
  }
}
