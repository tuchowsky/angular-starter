import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})

export class ApiService {
  apiData = new BehaviorSubject({});

  constructor(private http: HttpClient) { }

  getData(): void {
    this.http.get('https://reqres.in/api/users').subscribe(data => {
      // console.log(data);
      this.apiData.next(data);
    });
  }
}
