import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SideNavService {
  isNavOpenSource = new BehaviorSubject(true);
  isNavOpen = this.isNavOpenSource.asObservable();

  constructor() { }

  toggleNav(): void {
    this.isNavOpenSource.next(!this.isNavOpenSource.value);
    console.log(this.isNavOpenSource.value);
  }
}
