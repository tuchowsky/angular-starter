import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-container-todo',
  templateUrl: './todo.component.html',
  styleUrls: ['./todo.component.scss']
})
export class TodoContainerComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
