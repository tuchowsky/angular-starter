import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TodoContainerComponent } from './todo.component';

@NgModule({
  declarations: [TodoContainerComponent],
  imports: [
    CommonModule
  ],
  exports: [
    TodoContainerComponent
  ]
})
export class TodoModule { }
